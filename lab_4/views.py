from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    note = Note.objects.all().values()
    response = {'note':note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'note':note}
    return render(request, 'lab4_note_list.html', response)