from django.db import models

class Note(models.Model):
    To = models.CharField(max_length=60)
    From = models.CharField(max_length=60)
    Title = models.CharField(max_length=60)
    Message = models.CharField(max_length=60)
